require_relative '../../models/check_result.rb'

require 'pg'
require 'date'


class Projects::CheckCodesController < Projects::ApplicationController
    include ExtractsPath
    include ProductAnalyticsTracking
    include MembersPresentation

    # Authorize
    before_action :require_non_empty_project
    before_action :assign_ref_vars
    before_action :authorize_read_repository_graphs!
    
    MAX_COMMITS = 1000

    class CheckResult
        attr_accessor :id, :project_id, :commit_sha, :user_id, :ref_name, :blocker, :critical, :major, :minor, :info, :date, :author_email

        def initialize(id,project_id,commit_sha,user_id,ref_name,blocker,critical,major,minor,info,date,author_email)
            @id = id
            @project_id = project_id
            @commit_sha = commit_sha
            @user_id = user_id
            @ref_name = ref_name
            @blocker = blocker
            @critical = critical
            @major = major
            @minor = minor
            @info = info
            @date = date
            @author_email = author_email
        end

        def initialize(value_array)
            @id = value_array[0]
            @project_id = value_array[1]
            @commit_sha = value_array[2]
            @user_id = value_array[3]
            @ref_name = value_array[4]
            @blocker = value_array[5]
            @critical = value_array[6]
            @major = value_array[7]
            @minor = value_array[8]
            @info = value_array[9]
            @date = value_array[10]
            @author_email = value_array[11].sub(/.*<(.+)>.*$/, '\1')
        end
    end
    #Date.parse(date)
    class CommitLog 
        attr_accessor :author_name, :author_email, :date, :diff_line_count
        def initialize(author_name, author_email, date, diff_line_count)
            @author_name = author_name
            @author_email = author_email
            @date = Date.parse(date)
            @diff_line_count = diff_line_count
        end
    end
    
    

    def show
        @present_members=@project.members[0].user.name.inspect
        @all_members=@project.members
        @check_results = get_check_results()
        @commit_log=get_commits
        #整合数据
        @u_result=@check_results.group_by{|check_result| [check_result.author_email,check_result.date]}
        @u_result.transform_values! do |check_results|
            check_results.reduce({ blocker: 0,critical: 0,major: 0,minor: 0,info: 0, }) do |sums, check_result|
              sums[:blocker] += check_result.blocker || 0
              sums[:critical] += check_result.critical || 0
              sums[:major] += check_result.major || 0
              sums[:minor] += check_result.minor || 0
              sums[:info] += check_result.info || 0
              sums
            end
          end
        @u_commits=@commit_log.group_by{|commit_log| [commit_log.author_email,commit_log.date]}
        @u_commits.transform_values! do |commit_logs|
            commit_logs.reduce({diff_line_count:0}) do |sums,commit_log|
                sums[:diff_line_count] += commit_log.diff_line_count || 0
                sums
            end
        end

        #哈希表，键是一个数组["邮箱","日期"]，值又是一个哈希表{:blocker:值,:major:值....}
        @u_all=@u_result.merge(@u_commits) {|key, oldval, newval| oldval.merge(newval)}

        # @mt_res=@u_all.inspect
        #用户得分数组格式数组的数组：[["邮箱1","得分"],["邮箱2","得分"],...]
        @all_users=[]
        @u_all.each_key do |key|
            @all_users<<key[0]
        end
        #去重
        @all_users=@all_users.uniq
        @emali_name_map=@all_users.map{|i| [i]}
        #将所有元素变为数组，并添加初始得分
        @all_users=@all_users.map {|i| [i]<<0}
        
        for i in @emali_name_map do 
            for j in @check_results do 
                if i[0]==j.author_email
                    for k in @all_members do 
                        if j.user_id==k.user_id
                            i<<k.user.name
                            break
                        end
                    end
                    break
                end
            end
        end



        
        #项目质量得分变化算法：
        
        
        #u_all含有特定用户特定日期的得分使用[:score]获取
        for key,value in @u_all do
            b_c=0
            c_c=0
            ma_c=0
            mi_c=0
            i_c=0
            dff_c=0
            if value.has_key?(:blocker)
                b_c=value[:blocker]
                c_c=value[:critical]
                ma_c=value[:major]
                mi_c=value[:minor]
                i_c=value[:info]
            end
            if value.has_key?(:diff_line_count)
                dff_c=value[:diff_line_count]
                dff_c=1.0/(1.0+Math.exp(-1.0*dff_c/10))
            end
            score=(-0.5*b_c*dff_c)+(-0.1*c_c*dff_c)+(Math.exp(-1.0*ma_c/10)*dff_c)+(Math.exp(-1.0*mi_c/100)*dff_c)+(Math.exp(-1.0*i_c/1000)*dff_c)
            value["score"]=score
        end

        #统计各种错误和行数总数
        @total_blocker=0
        @total_critical=0
        @total_major=0
        @total_minor=0
        @total_info=0
        for key,value in @u_all do 
            if value.has_key?(:blocker)
                @total_blocker+=value[:blocker]
                @total_critical+=value[:critical]
                @total_major+=value[:major]
                @total_minor+=value[:minor]
                @total_info+=value[:info]
            end
        end

        #用户分数计算
        for key,value in @u_all do
            for i in @all_users do
                if key[0]==i[0]
                    i[1]+=value["score"]
                end
            end
        end
        #日期分数计算
        end_date=Time.new
        end_date.strftime("%Y-%m-%d")
        start_time=@u_all.keys[0][1]
        #创建从第一次提交到当前日期的时间数组[["日期",到该日期为止的项目质量得分],[...]]
        @perday_score=(start_time..end_date).map(&:to_s)
        @perday_score=@perday_score.map {|i| [i]<<0}
        #创建一个随时间的进度数组[["日期",与前一天相比项目质量得分变化值],...]
        @process_score=(start_time..end_date).map(&:to_s)
        @process_score=@process_score.map {|i| [i]<<0}
        #创建一个时间的提交行数数组[["日期",当天提交的代码行数],...]
        @day_count=(start_time..end_date).map(&:to_s)
        @day_count=@day_count.map {|i| [i]<<0}

        @perday_score.each_with_index do |value,index|
            if index > 0
                @perday_score[index][1]=@perday_score[index-1][1]
            end
            for key,val in @u_all do
                this_date=Date.parse(value[0])
                if this_date==key[1]
                    value[1]+=val["score"]
                end
            end
        end

        @perday_score.each_with_index do |value,index|
            if index > 0
                @process_score[index][1]=@perday_score[index][1]-@perday_score[index-1][1]
            end
        end

        @day_count.each_with_index do |value|
            for key,val in @u_all do
                this_date=Date.parse(value[0])
                if this_date==key[1]
                    if val.has_key?(:diff_line_count)
                        value[1]+=val[:diff_line_count]
                    end
                end
            end
        end
        #总天数
        day_num=0
        #总共提交代码行数
        @total_count=0
        for value in @day_count do 
            day_num=day_num+1
            @total_count+=value[1]
        end
        mean_count=(@total_count*1.0)/day_num
        #方差
        variance=0.0
        for value in @day_count do
            variance+=(value[1]-mean_count)**2
        end
        variance=Math.sqrt(variance)
        #团队推进评价分数
        @evaluation_score=Math.exp(-1.0*variance/1000)*100
        @evaluation_score=@evaluation_score.inspect

        #团队贡献度算法
        total_score=0
        for i in @all_users do 
            total_score+=i[1]
        end
        @user_contri=[]
        for i in @all_users do 
            temp=[]
            temp<<i[0]
            temp<<(i[1]*1.0/total_score)*100
            @user_contri<<temp
        end

        @total_issue=[@total_count,@total_blocker,@total_critical,@total_major,@total_minor,@total_info].inspect
        
        @perday_score=@perday_score.inspect
        @process_score=@process_score.inspect
        @day_count=@day_count.inspect
        
        
        @mt_res=@u_all.inspect

        
        @pro_info = @project.inspect
        #邮箱用户名映射
        @emali_name_map.each_with_index do |value,index|
            @all_users[index][0]=value[1]
            @user_contri[index][0]=value[1]
        end

        
    end

    private

    def init_database
        @connection = PG.connect(host:'121.36.49.74',dbname: 'scanner', user: 'zw')
        @connection.type_map_for_results = PG::BasicTypeMapForResults.new(@connection)
    end

    def select_check_results_from_database
        if !defined?(@connection)
            init_database()
        end
        @connection.exec("SELECT * FROM check_results WHERE project_id="+@project.id.to_s )
    end

    def get_check_results
        pg_results=select_check_results_from_database.values
        pg_results.map do |result|
            CheckResult.new(result)
        end
    end

    def get_commits
        commits = @project.repository.commits(@ref, limit: MAX_COMMITS, skip_merges: true)
        commits.map do |commit|
            CommitLog.new(commit.author_name,commit.author_email,commit.committed_date.strftime("%Y-%m-%d"),commit.diff_line_count)
        end
    end
    

end