import Vue from 'vue';
import { waitForCSSLoaded } from '~/helpers/startup_css_helper';
import { __ } from '~/locale';
import rawData from './components/rawData.vue'
import chart from './components/chart.vue'

waitForCSSLoaded(() => {
    const rawDataSelector = document.getElementById('check-code-raw-data');
    const chartSelector = document.getElementById('check-code-chart') 
    const LANGUAGE_CHART_HEIGHT = 300;
    const HEATMAP_CHART_HEIGHT = 300;
    console.log(chartSelector.dataset)
    console.log(rawDataSelector.dataset)
    new Vue({
      el: rawDataSelector,
      components: {
        rawData
      },
      data() {
        return {
          check_results: JSON.parse(rawDataSelector.dataset.checkResults) ,
          commit_log: JSON.parse(rawDataSelector.dataset.commitLog)
        }
      },
      render(h) {
        return h(rawData, {
          props: {
            check_results: this.check_results,
            commit_log: this.commit_log
          }
        })
      }
    });
    new Vue({
      el: chartSelector,
      components: {
        chart
      },
      data() {
        return {
          user_score_bar:JSON.parse(chartSelector.dataset.userScoreBar),
          project_score_line: JSON.parse(chartSelector.dataset.projectScoreLine),
          user_contribute_pie:JSON.parse(chartSelector.dataset.userContributePie),
          process_score_heatmap:JSON.parse(chartSelector.dataset.processScoreHeatmap),
          evaluation_score_number:JSON.parse(chartSelector.dataset.evaluationScoreNumber),
          total_issue:JSON.parse(chartSelector.dataset.totalIssue)
        }
       },
      render(h) {
        return h(chart, {
          props: {
            user_score_bar: this.user_score_bar,
            project_score_line: this.project_score_line,
            user_contribute_pie: this.user_contribute_pie,
            process_score_heatmap: this.process_score_heatmap,
            evaluation_score_number: this.evaluation_score_number,
            total_issue: this.total_issue
          }
        })
      }
    })
 

})